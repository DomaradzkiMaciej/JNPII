#include "scalar_product.h"
#include "functions.h"

int main(int argc, char** argv) {
    const int ROWS = 145;
    real* CNV[ROWS];
    
    int len;
    if (argc == 2) {
        // ReadCSV returns the length of the vectors. It also fills two-dimensional array CNV.
        // The rows of CNV are allocated in the function.
        len=ReadCSV(argv[1],CNV);
        similarity_gpu_grid(CNV,ROWS,len);

        return(0);
    }
    else {
        printf("Wrong number of arguments\n");
        printf("Usage: %s filename\nExiting\n",argv[0]);
        exit(1);
    }
}

static int fatal(cudaError_t status) {
    cout << cudaGetErrorString(status) << endl;
    exit(1);
}

static real *flat_matrix(real** sourceMat, int Size, int Len) {
    int indx;
    real *cpuMat;
    cudaError_t status;

    time_t start;
    time_t end;

    start = clock();

    // Make a local copy of the CPU matrix
    // It will be easier to copy it in a single operation to GPU
    status = cudaMallocHost((void**) &cpuMat, sizeof(real) * Size*Len);
    if (status != cudaSuccess) 
        fatal(status);

    // copy vectors from sourceMat to cpuMat
    indx=0;
    for (int i=0; i < Size; i++) {
        for (int j=0; j<Len; j++) {
            cpuMat[indx] = sourceMat[i][j];
            indx++;
        }
    }

    end = clock();
    printf("\n Matrix flatting %lf s\n\n", 1.0*(end-start)/CLOCKS_PER_SEC);

    return cpuMat;
}

static real *copy_on_gpu(real *cpuMat, int Size, int Len) {
    real *gpuMat;
    cudaError_t status;

    status = cudaMalloc((void**) &gpuMat, sizeof(real) * Size*Len);
    if (status != cudaSuccess) 
        fatal(status);

    
    status = cudaMemcpy(gpuMat, cpuMat, sizeof(real) * Size*Len, cudaMemcpyHostToDevice);
    if (status != cudaSuccess)
        fatal(status);

    return gpuMat;
}

static real *doTransposition(real *gpuMat, int &Size, int &Len) {
    int aux;
    real *auxGpuMat;
    cudaError_t status;

    status = cudaMalloc((void**) &auxGpuMat, sizeof(real) * Size*Len);
    if (status != cudaSuccess) 
        fatal(status);

    transposition<<< Size/32+1, 32,0 >>>(gpuMat, auxGpuMat, Len, Size);                                 
    aux = Len;
    Len = Size;
    Size = aux;

    status = cudaFree(gpuMat);
    if (status != cudaSuccess)
        fatal(status);

    return auxGpuMat;
}

static void doNormalization(real *gpuMat, int Size, int Len) {
        normalizeGPU<<<Size/64+1, 64>>>(gpuMat, Len, Size);
}

static void doScalar(real *gpuMat, int Size, int Len){
    int block_size_x = 32;
    int block_size_y = 32;
    int grid_size_x = Size/block_size_x + 1;
    int grid_size_y = Size/block_size_y + 1;

    real *simMat_cpu;
    real *simMat_gpu;

    dim3 threadsPerBlock(block_size_x, block_size_y,1); 
    dim3 numBlocks(grid_size_x, grid_size_y,1); 
    cudaError_t status;

    time_t start;
    time_t end;

    start = clock();

    status = cudaMalloc((void**) &simMat_gpu, sizeof(real) * Size);
    if (status != cudaSuccess)
        fatal(status);

    // Allocate a local CPU similarity matrix
    status = cudaMallocHost((void**) &simMat_cpu, sizeof(real) * Size);
    if (status != cudaSuccess) 
        fatal(status);
            
    scalar<<< numBlocks, threadsPerBlock, 0 >>>(gpuMat, Len, Size, simMat_gpu );                                 
           
    status = cudaMemcpy(simMat_cpu, simMat_gpu, sizeof(real)* Size, cudaMemcpyDeviceToHost);
    if (status != cudaSuccess)
        fatal(status);
            
    status = cudaFreeHost(simMat_cpu);
    if (status != cudaSuccess)
        fatal(status);

    status = cudaFree(simMat_gpu);
    if (status != cudaSuccess)
        fatal(status); 
        
    end = clock();
    printf("\n Scalar %lf s\n\n",1.0*(end-start)/CLOCKS_PER_SEC);

}

void similarity_gpu_grid(real** sourceMat, int Size, int Len) {
    real *cpuMat;
    real *gpuMat;
    cudaError_t status;
    
    time_t start;
    time_t end;
    
    start = clock();

    cpuMat = flat_matrix(sourceMat, Size, Len);
    gpuMat = copy_on_gpu(cpuMat, Size, Len);
    gpuMat = doTransposition(gpuMat, Size, Len);
    doNormalization(gpuMat, Size, Len);
    doScalar(gpuMat, Size, Len);

    status = cudaFreeHost(cpuMat);
    if (status != cudaSuccess)
        fatal(status);

    status = cudaFree(gpuMat);
    if (status != cudaSuccess)
        fatal(status);

    end = clock();
    printf("\n All gpu related computation %lf s\n\n",1.0*(end-start)/CLOCKS_PER_SEC);
}

__device__ real scalarDev(real *x,int Len) {
    real sum = 0.0;
    for (int i=0;i<Len;i++) {
        sum+= x[i]*x[i];
    }

    return sum;
}

__global__ void normalizeGPU(real* Mat, int Len, int Size) {
    int s_x = (blockIdx.x * blockDim.x + threadIdx.x) * Len;
    real Sum;

    if (s_x < Size * Len) {
        Sum=scalarDev(Mat + s_x, Len);
        Sum=sqrt(Sum);

        for (int j=0;j<Len;j++) {
            Mat[s_x + j] /= Sum;
        }
    }
}

__global__ void scalar(real *Mat, int Len, int Size, real *resMat ){
    int s_x = blockIdx.x * blockDim.x + threadIdx.x;
    int s_y = blockIdx.y * blockDim.y + threadIdx.y;
    int id_x = threadIdx.x;
    int id_y = threadIdx.y+blockDim.x;

    real res = 0.0;
    __shared__ real buffer[64][33]; // 33 zapobiega konfliktom pamięci, bo powoduje przesunięcie indexów o 1 bank 

    for (int i=0; i < Len; i += 32) {
        if (s_x < Size){
            for (int j = threadIdx.y; j < 32 && j+i < Len; j += blockDim.y){
                buffer[id_x][j] = Mat[Len*s_x + i+j];
            }
        }

        if (s_y < Size){
            for (int j = threadIdx.x; j < 32 && j+i < Len; j += blockDim.x){
                buffer[id_y][j] = Mat[Len*s_y + i+j];
            }

        }
        __syncthreads();
    
        if (s_x < Size && s_y < Size && s_x <= s_y){
            for (int j = 0; j<32 && i+j < Len; j++) {
                res += buffer[id_x][j]*buffer[id_y][j];
            }
        } 
    
        __syncthreads();
    }

    // %Size aby jedynie zasymulować zapisywanie, bo tablica Size*Size byłaby zdecydowanie za duża
    if (s_x < Size && s_y < Size && s_x <= s_y){
        int loc_indx1 = s_x*Size+s_y;
        int loc_indx2 = s_y*Size+s_x;
        resMat[loc_indx1%Size]=res;
        resMat[loc_indx2%Size]=res;
    }
}

__global__ void transposition(real *Mat, real *resMat, int Len, int Size){
    int s_x = blockIdx.x * blockDim.x + threadIdx.x;
    int id_x = threadIdx.x;

    __shared__ real buffer[32][33]; // 33 zapobiega konfliktom pamięci, bo powoduje przesunięcie indexów o 1 bank 

    for (int i=0; i < Len; i += 32) {
        if (s_x < Size){
            for (int j = threadIdx.y; j < 32 && j+i < Len; j += 1){
                buffer[id_x][j] = Mat[Len*s_x + i+j];
            }
        }

        __syncthreads();
    
        
        for (int j = threadIdx.y; j < 32; j += 1){
            int position = Size*(i+id_x) + s_x-id_x+j;

            if (s_x-id_x+j < Size && i+id_x < Len){
                resMat[position] = buffer[j][id_x];
            }
        }
    
        __syncthreads();
    }
}
