int ReadCSV(char* CSVfile,real** CNV) {
// We know the size of the data, we define sufficiently large buffers for this case.
//
    FILE* CNVfile;
    real* row;
    const int buf_size=2000000;
    const int max_cols=100000;
    char buffer[buf_size];
    char* tmp;
    int col_count;
    
    row = (real*) malloc(max_cols*sizeof(real));
    printf("Reading %s\n",CSVfile);
    CNVfile = fopen(CSVfile,"r");
    int line_count =0;
    
    int row_count;
    while ((tmp=fgets(buffer, 1999999, CNVfile))!=NULL){
        line_count++;
        if (line_count>1){
            //we skip header line, hence we start from line_count = 2 here.
            row_count = line_count-2;
            col_count=-1;
            char *col = strtok(buffer, ",");
            while (col) {
                if (col_count >= 0) {
                    row[col_count]=atof(col);
                }
                col = strtok(NULL, ",");
                col_count++;
            }
            CNV[row_count]= (real*) malloc((col_count+1)*sizeof(real));
            for (int i=0;i<=col_count;i++) CNV[row_count][i]=row[i];
        }
    }
    fclose(CNVfile);
    return(col_count);
}

real scalar(real *x, real* y,int len){
    //printf("Multiplying vectors\n");
    real sum=0.0;
    for (int i=0;i<len;i++) sum+= x[i]*y[i];
    return(sum);
}


double scalar_d(real *x, real* y,int len){
    //printf("Multiplying vectors\n");
    double sum=0.0;
    for (int i=0;i<len;i++) sum+= x[i]*y[i];
    return(sum);
}

void normalize(real** sourceMat,real**  destMat, int Size, int Len) {
    int i,j;
    real Sum;
    // Zdefiniuj timery
    time_t start, end;
    // do computations
    printf("function normalize():\n");
    start=clock();
    for (i=0;i<Size;i++){
        Sum=scalar(sourceMat[i],sourceMat[i],Len);
        Sum=sqrt(Sum);
        for (j=0;j<Len;j++) destMat[i][j]=sourceMat[i][j]/Sum;
        //printf("%8.4f ",Sum);
    }
    end=clock();
    printf("computations took %lf s\n\n",1.0*(end-start)/CLOCKS_PER_SEC);    
}

void normalize_d(real** sourceMat,real**  destMat, int Size, int Len) {
    int i,j;
    double Sum;
    real Sum_f;
    // Zdefiniuj timery
    time_t start, end;
    // do computations
    start=clock();
    for (i=0;i<Size;i++){
        Sum=scalar_d(sourceMat[i],sourceMat[i],Len);
        Sum_f= (real) sqrt(Sum);
        for (j=0;j<Len;j++) destMat[i][j]=sourceMat[i][j]/Sum_f;
        printf("%8.4lf ",Sum);
    }
    end=clock();
    printf("\nComputations took %lf s\n\n",1.0*(end-start)/CLOCKS_PER_SEC);
}

double correlation(real *x,real *y,int Size){
    double Sumx, Sumy, Sumxy, Sumxx, Sumyy; 
    double mx, my; 
    double tx, ty;
    double r;

    Sumx =0;
    Sumy =0;
    Sumxy =0;
    Sumxx=0;
    Sumyy=0;

    for (int i=0; i<Size;i++) {
        tx = x[i];
        ty = y[i];
        Sumx += tx;
        Sumy += ty;
        Sumxy += tx*ty;
        Sumxx += tx*tx;
        Sumyy += ty*ty;
    }
    mx = Sumx/Size;
    my = Sumy/Size;
    r = (Sumxy - Size*mx*my)/(sqrt( (Sumxx -Size*mx*mx)*(Sumyy-Size*my*my) ));
    return(r);
}

void similarity(real** sourceMat, real* simMat, int Size, int Len){
    int i,j;
    real Sum, Min, Max;
    Sum=Max=0.0f;
    Min=1.0f;
    time_t start, end;
    // do computations
    printf("function similarity()\n:");
    start=clock();
    for (i=0;i<Size;i++) {
        for (j=0;j<Size;j++) {
            simMat[i*Size+j]=scalar(sourceMat[i],sourceMat[j],Len);
            //Sum+=simMat[i*Size+j];
            //if (Min>fabs(simMat[i*Size+j])) Min=fabs(simMat[i*Size+j]);
            //if ( (i!=j) & (Max<simMat[i*Size+j]) ) Max=simMat[i*Size+j];
        }
    }
    end=clock();
    printf("\ncomputations took %lf s\n\n",1.0*(end-start)/CLOCKS_PER_SEC);
    printf("Minimum similarity(%%): %lf\n",Min*100);
    printf("Maximum similarity(%%): %lf\n",Max*100);
    printf("Average similarity(%%): %lf\n",(Sum-Size)/(Size*(Size-1))*100);
}

void showMetrics(real* simMat, int Size) {
real Min =1.0;
real  Max =0.0;
double Sum = 0;
real r, r2;
int i,j;

    for (i=0;i<Size;i++) {
        for (j=i+1;j<Size;j++) {
            r = simMat[i*Size+j];
            r2 = r*r;
            if (r2 < Min) Min=r2;
            if (r2>Max) Max = r2;
            Sum += r2;
        }
    }
    printf("Minimum similarity(%%): %lf\n",Min*100);
    printf("Maximum similarity(%%): %lf\n",Max*100);
    printf("Average similarity(%%): %lf\n",Sum/(Size*(Size-1))/2*100);
}
