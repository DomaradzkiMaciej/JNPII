#ifndef scalar_product_h
    #define scalar_product_h
    #ifndef DEBUG 
        #define DEBUG 0
    #endif 
    #include <cuda_runtime_api.h>
    using namespace std;
    #include <iostream>
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <time.h>
    #include <math.h>
    #ifndef real
        #define real float
    #endif
    #define BlockSize 1024

#endif /* scalar_product_h */

int ReadCSV(char* CSVfile,real** CNV);
real scalar(real *x, real* y, int len);
void normalize(real** sourceMat,real**  destMat, int Size, int Len);
void similarity(real** sourceMat, real* simMat, int Size, int Len);

double scalar_d(real *x, real* y, int len);
void normalize_d(real** sourceMat,real**  destMat, int Size, int Len);
void similarity_gpu(real** sourceMat, real* simMat, int Size, int Len);
double correlation(real *x,real *y,int Size);
void similarity_gpu_grid(real** sourceMat, int Size, int Len);
__device__ real scalarDev(real *x,int len);
__global__ void normalizeGPU(real* Mat, int Len, int Size);
__global__ void transposition(real *Mat, real *resMat, int Len, int Size);
__global__ void scalar(real *Mat, int Len, int Size, real *resMat); 
void showMetrics(real* simMat, int Size);